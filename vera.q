@c -*-texinfo-*-
@c This is part of the GNU edition of V.E.R.A.
@c Copyright (C) 1993/2020 Oliver Heidelbach
@c See the file vera.texi for copying conditions.
@c 
@c Syntax:
@c ACRONYM
@c Expansion[ (Reference)][, "Style"]
@c Additional explanations are included in [square brackets]

@table @asis
@item QA
Quality Assurance

@item QA
Question & Answers, "Q&A"

@item QA
Quick Arbitration (HD)

@item QAM
Quadrature Amplitude Modulation (HiperLAN/2, 802.11a)

@item QAP
Quality-Assurance Program (QA)

@item QAPI
Quality-Assurance Program Inspections (QA)

@item QAPI
Queue Application Program Interface (SAP, API)

@item QAPP
Quality-Assurance Program Plan (QA)

@item QAS
Quasi-Associated Signaling

@item QAT
Quick Assist Technology (Intel)

@item QBASIC
Quick Beginner's All-purpose Symbolic Instruction Code (BASIC, DOS)

@item QBBS
Quick Bulletin Board System (BBS)

@item QBE
Query By Example (DB)

@item QBF
Query By Forms (DB)

@item QBH
Query By Humming (DB, audio), "QbH"

@item QBIC
Query By Image Content (IBM)

@item QBS
Query By Struct (DB)

@item QC
Quad Core (CPU)

@item QCA
Quantum-dot Cellular Automata

@item QCB
Queue Control Block

@item QCDP
Quality Color Dithering Modus

@item QCIF
Quarter Common Interchange Format [176 x 144]

@item QCIF
Quarter Common Intermediate Format [176x144] (video)

@item QCM
Quad Core Modul (IBM, DCM)

@item QCT
Quanta Cloud Technologies

@item QD
Queueing Delay

@item QDEF
Quantum Dot Enhancment Film (3M)

@item QDL
Quadri Data Layer [CD] (CD)

@item QDOS
Quick and Dirty Operating System (OS, PC, MS-DOS, predecessor)

@item QDP
Quad-Die -Package

@item QDR
Quad Data Rate (SDR, GDR, ODR)

@item QEMM
Quarterdeck Expanded Memory Manager (EMM, Quarterdeck)

@item QEMU
Quick EMUlator (Linux)

@item QF
Query Filter (DB)

@item QFA
Quick File Access

@item QFE
Quick Fix Engineering (MS)

@item QFHD
Quad Full High Definition

@item QFP
Quad Flat Pack

@item QHD
Quarter High Definition [640x360], [960x540] (HD)

@item QIC
Quarter Inch Committee (org., Streamer)

@item QICSC
Quarter Inch Cartridge Standards Committee (QIC)

@item QICW
Quarter Inch Cartridge Wide (QIC)

@item QIS
Quality Information System

@item QL
Query Language

@item QLC
Quadruple Level Cell (Flash)

@item QLD
Queuing Literature Database (DB)

@item QLI
Query Language Interpreter

@item QLLC
Qualified Logical Link Control [protocol] (IBM)

@item QMB
Quad Band Memory (IC)

@item QMC
Quine McCluskey approach

@item QMF
Query Management Facility (IBM)

@item QML
Qt Meta / Modeling Language

@item QMR
Quarterly Maintenance Releases (Lotus)

@item QMS
Queue Management Service (Netware)

@item QOS
Quality Of Service (ATM, CLR, CTD, CDV), "QoS" 

@item QP
Queue Pair (Infiniband, HCA)

@item QPA
Quality Program Analysis

@item QPE
Qtopia Palmtop Environment (PDA)

@item QPEL
Quarter PixEL (DivX, video), "QPel"

@item QPI
Quick Path Interconnect

@item QPI
Quick Path Interface

@item QPL
Q Public License

@item QPS
Quark Publishing System (DTP)

@item QPSK
Quadri Phase Shift Keying [modulation] (HiperLAN/2, , 802.11a)

@item QPSX
Queue Packet and Synchronous circuit Exchange

@item QPU
Quick Pascal Units (MS)

@item QR
Quick Response [code]

@item QSA
Qt Script for Applications

@item QSA
Query String Append (HTTPD, Apache)

@item QSAM
Queued Sequential Access Method (SAM)

@item QSU
Quatech Serial to USB [adapter] (USB)

@item QSVGA
Quarter SVGA [400x300] (SVGA, VGA)

@item QSXGA
Quad Super XGA [2560x2048] (XGA)

@item QTAM
Queued Terminal Access Method

@item QTC
Quantum Tunneling Composite [polymer]

@item QTE
Quick-Time-Event

@item QTVR
QuickTime Virtual Reality (Apple, VR)

@item QUEST
Quality Excellence for Suppliers of Telecommunication Leadership (org.), "QuEST"

@item QUEX
Query Update by EXample

@item QUHD
Quad Ultra HD [15360x8640] (HD)

@item QUIC
Quick UDP Internet Connections (UDP, Google)

@item QUICC
QUad Integrated Communications Controller (Motorola)

@item QUIP
QUad In-line Package

@item QUIPS
QUality Improvements Per Second

@item QUXGA
Quad Ultra XGA [3200x2400] (XGA, HSVGA)

@item QVGA
Quarter Video Graphics Array [320 x 240] (VGA)

@item QWAVE
Quality Windows Audio Video Experience (MS, Windows)

@item QWUXGA
Quad Wide Ultra XGA [3840x2400] (XGA, WQUXGA)

@item QXGA
Quantum Extended Graphics Array [2048 x 1536]

@item QXI
Queue eXecutive Interface

@end table

