@c -*-texinfo-*-
@c This is part of the GNU edition of V.E.R.A.
@c Copyright (C) 1993/2020 Oliver Heidelbach
@c See the file vera.texi for copying conditions.
@c 
@c Syntax:
@c ACRONYM
@c Expansion[ (Reference)][, "Style"]
@c Additional explanations are included in [square brackets]

@table @asis
@item W2K
Windows 2000 (Windows, MS)

@item W3C
World Wide Web Consortium (WWW, org.)

@item WABI
Windows Application Binary Interface (MS, Windows)

@item WAFL
Write Anywhere File Layout (Netapp)

@item WAFS
Wide Area File Services

@item WAG
Wild-Assed Guess (slang, Cygwin)

@item WAI
Web Accessibility Initiative (WWW, W3C, WAI)

@item WAI
Web Application Interface (WWW, Netscape)

@item WAIK
Windows Automated Installation Kit (MS, Windows, Vista)

@item WAIS
Wide Area Information Service (Internet)

@item WAITS
Westcoast Alternative to ITS

@item WAM
Warren Abstract Machine (PROLOG)

@item WAM
Web Application Manager (MS, Windows)

@item WAM
Wikia Activity Monitor

@item WAM
Windows Animation Manager (MS, Windows)

@item WAMIS
Wireless, Adaptive and Mobile Information Systems (ESTO)

@item WAMP
Windows, Apache, MySQL, PHP (Windows, Apache, PHP, DB, SQL)

@item WAMPP
Windows, Apache, MySQL, PHP and PERL (Windows, Apache, DB, SQL, PHP, PERL)

@item WAN
Wide Area Network

@item WAND
Wide Area Network Distribution (WAN)

@item WAP
Wireless Application Protocol (mobile-systems, WLAN)

@item WAP
Wissenschaftliche ArbeitsPlatzrechner

@item WAPP
Windows, Apache, PostgreSQL, PHP (Windows, Apache, PHP, DB, SQL)

@item WAR
Web Application aRchive (Java)

@item WAR
Write-After-Read (WAW, RAW)

@item WARP
Windows Advanced Rasterization Platform (MS, Windows)

@item WAS
Websphere Application Server (IBM, WBISF)

@item WASC
Web Application Security Consortium (org., WWW)

@item WASN
Wide Area Sensor Network

@item WASP
Web stAndardS Project (WWW, W3C)

@item WAT
West Africa Time [-0100] (TZ)

@item WATS
Wide Area Telephone Service

@item WAW
Write-After-Write (WAR, RAW)

@item WB
WorkBench (Amiga, Commodore)

@item WBC
Web Based Collaboration (WWW)

@item WBC
Wide Band Channel (FDDI)

@item WBEM
Web Based Enterprise Management [protocol] (DMTF, CIM, XML, HTTP, BMC, Cisco, Compaq, Intel, MS)

@item WBF
Windows Biometric Framework (MS, Windows)

@item WBI
Web Browser Intelligence (WWW, IBM)

@item WBISF
Websphere Business Integration Server Foundation (IBM, BPEL)

@item WBS
WissensBasierte Systeme (KI)

@item WC
WildCard multicast route entry (PIM, Multicast)

@item WC
Word Count (Unix)

@item WC
Write Cache (SCSI, HDD)

@item WCA
Wikimedia Chapters Association (org.)

@item WCAG
Web Content Accessibility Guidelines (WAI, W3C)

@item WCB
Write Combining Buffer (CPU)

@item WCCE
World Conference on Computers in Education (IFIP, conference)

@item WCDMA
Wideband Code Division Multiple Access (mobile-systems)

@item WCDMAFDD
Wide-band Code Division Multiple Access - Frequency Division Duplex (UMTS, mobile-systems), "WCDMA-FDD"

@item WCDMATDD
Wide-band Code Division Multiple Access - Time Division Duplex (UMTS, mobile-systems), "WCDMA-TDD"

@item WCF
Windows Communication Foundation (MS, Windows)

@item WCGA
World Computer Graphics Association (org.)

@item WCIT
World Conference on International Telecommunication (ITU)

@item WCMS
Web Content Management Software / System (CMS)

@item WCS
Windows Color System (MS, Windows)

@item WCS
Writable Control Store (VAX, DEC)

@item WCST
Webcam Child Sex Tourism

@item WCTS
Without Consulting The Source (slang, Cygwin)

@item WD
Western Digital [corporation] (manufacturer)

@item WD
Working Draft

@item WDG
Web Design Group (WWW, org.)

@item WDK
Word Developers Kit (MS)

@item WDL
Windows Driver Library (MS, Windows)

@item WDL
World Definition Language

@item WDM
Wavelength Division Multiplexing [protocol]

@item WDM
Windows / Win32 Driver Model (MS, Windows)

@item WDMCSA
Windows Driver Model Connection and Streaming Architecture (MS, Windows, WDM)

@item WDP
WatchDog Process

@item WDRP
Whois Data Reminder Policy (ICANN)

@item WDS
WatchDog Server

@item WDS
Windows Deployment Service (MS, Windows)

@item WDS
Wireless Distribution System (WLAN)

@item WDS
Wireless Domain Service (Cisco, WLAN)

@item WDT
Watch Dog Timer

@item WDT
Western [European] Daylight Time [+0100] (TZ, WET)

@item WEBDAV
Web Distributed Authoring and Versioning (WWW, HTTP, RFC 2518), "webDAV"

@item WEBRC
Wave and Equation Based Rate Control

@item WEBRTC
WEB Real Time Communication (WWW, W3C), "WebRTC"

@item WECA
Wireless Ethernet Compatibility Alliance (org., WLAN, LAN)

@item WEEB
Western Europe EDIFACT Board (org., EDIFACT), "WE/EB"

@item WEEE
Waste from Electrical and Electronic Equipment (Europe)

@item WEFT
Web Embedding Font Tool (MS, WWW)

@item WELL
Whole Earth 'Lectronic Net (network)

@item WEP
Wired Equivalent Privacy (WLAN)

@item WESA
World ESports Association (org.)

@item WESTNET
WESTern regional NETwork (network, USA), "Westnet"

@item WET
Western European Time [+0000] (TZ, WDT)

@item WF
[Windows] Workflow Foundation (MS, Windows)

@item WFA
WiFi Alliance (org., WiFi)

@item WFC
Wait for Caller (BBS)

@item WFC
Windows Foundation Classes (MS, Java, API)

@item WFM
Wired For Management (Intel, IBM, BIOS), "WfM"

@item WFM
Works For Me (slang, Usenet, IRC)

@item WFMC
WorkFlow Management Coalition (org.), "WfMC"

@item WFP
Windows Filtering Platform (MS, Windows)

@item WFQ
Weighted Fair Queueing (VoIP)

@item WFS
Web Feature Service (OGC)

@item WFW
Windows For Workgroups (MS), "WfW" 

@item WG
Working Group (SC, ISO, IEC, ETSI)

@item WGA
Windows Genuine Advantage [program] (MS, Windows)

@item WHAT
Web Hypertext Applications Technology [working group] (WWW, org.)

@item WHATWG
Web Hypertext Application Technology Working Group (WWW, org.)

@item WHCA
White House Communications Agency (DISA)

@item WHDI
Wireless Home Digital Interface (org.)

@item WHEA
Windows Hardware Error  Architecture (MS, Windows)

@item WHIIG
Windows Hardware Instrumentation Implementation Guidelines (MS, Windows)

@item WHOLIS
World Health Organization Library Information System (org., UNO)

@item WHOSIS
World Health Organization Statistical Information System (org., UNO)

@item WHQL
Windows Hardware Quality Lab (MS, Windows, PC97)

@item WHSXGA
Wide Hex Super XGA [6400x4096] (XGA)

@item WHUXGA
Wide Hex Ultra XGA [7680x4800] (XGA)

@item WHXGA
Wide Hex XGA [5120x3200] (XGA)

@item WHYLINE
Workspace for Helping You Link Instructions to Numbers and Events (EuropeSES)

@item WIA
Windows Image Acquisition (MS, Windows, API, DDI)

@item WIA
Windows Imaging Architecture (MS, Windows, Vista)

@item WIC
Windows Imaging Component (MS, Windows)

@item WICG
Web platform Incubator Community Group (org., W3C)

@item WIDD
Web InformationsDienst Deutschland (WWW, Neuss, Germany)

@item WIDI
WIreless DIsplay (Intel), "WiDi"

@item WIF
Web Interface Facility (WWW, MVS, OS/390)

@item WIFI
WIreless FIdelity [alliance] (WLAN), "Wi-Fi"

@item WIFI
WIreless FIdelity [certificate] (WECA, WLAN), "WiFi"

@item WIIS
Windows Internet Information Server (MS, Windows, Internet)

@item WIKINGER
WIKI Next Generation Enhancement Repository

@item WIM
Wireless Identity Module (WAP, cryptography)

@item WIMA
[windows] Webserver - IIS - MSDE - ASP.Net (MS, IIS, MSDE, ASP)

@item WIMAN
WIreless Metropolitan Area Network (MAN, W-LAN)

@item WIMAX
Worldwide Interoperability for Microwave ACCess (IEEE 802.16)

@item WIMP
Window, Icon, Menu, Pointing device

@item WIN
WIssenschaftsNetz (network, DFN)

@item WINHEC
WINdows Hardware Engineering Conference (MS, Windows, conference), "WinHEC"

@item WINLAB
Wireless Information Network LABoratory (org., STC, USA)

@item WINNIE
Water Initiated Numerical Number Integrating Engine

@item WINRE
WINdows Recovery  Environment (MS, Windows), "Win RE"

@item WINS
Windows Internet Naming Service (MS, Windows NT)

@item WIP
Work In Progress (slang)

@item WIPO
World Intellectual Property Organization (org.)

@item WIPS
Web Interactions Per Second (TPC)

@item WIPSB
Web Interactions Per Second Browsing (TPC, WIPS), "WIPSb"

@item WIPSO
Web Interactions Per Second Ordering (TPC, WIPS), "WIPSo"

@item WISA
Wireless Speaker and Audio Association (org.)

@item WISC
Wisconsin Integrally Synchronized Computer

@item WISE
World-wide Information System for r&d Efforts (WWW, IGD)

@item WISH
Wireless Intelligent Stream Handling (W-LAN)

@item WISIA
Wissenschaftliches InformationsSystem fuer den Internationalen Artenschutz (WWW)

@item WISPR
Wireless ??? ISP Roaming (WLAN, ISP, org., WECA), "WISPr"

@item WITCH
Wolverhampton Instrument for Teaching Computation from Harwell

@item WITT
Workstation Interactive Test Tool (IBM)

@item WIX
Windows Installer XML (MS, Windows, XML), "WiX"

@item WIZOP
WIzard sysOP, "WizOp" 

@item WKS
Well Known Services (DNS, Internet)

@item WLAN
Wireless Local Area Network (LAN, WLAN, IEEE)

@item WLANA
Wireless Local Area Network Alliance (org., WLAN, LAN)

@item WLBS
Windows Load Balancing Service (MS, Windows NT)

@item WLC
Workload License Charge (IBM, PSLC)

@item WLCG
Worldwide LHC Computing Grid (CERN)

@item WLCSP
Wafer-Level ChipScale Packaging

@item WLIF
Wireless LAN Interoperability Forum (org., WLAN, LAN)

@item WLL
Wireless Local Loop

@item WLM
WorkLoad Manager (IBM, HP-UX)

@item WLO
Windows Libraries for OS/2 (MS, IBM, API, OS/2, Windows)

@item WLO
Windows Library Objects

@item WLOG
Without Loss Of Generality

@item WLP
Wafer Level Packaging

@item WLS
White Line Skip (Fax)

@item WLSEC
Wireless LAN SECurity [framework], WLSec

@item WMA
Windows Media Audio [CODEC] (MS, Windows, audio, CODEC)

@item WMA
Wireless Messaging API (API, Java, J2ME, JSR-120)

@item WMAN
Wireless Metropolitan Area Network (WLAN, MAN)

@item WMC
Windows Media Connect (MS, Windows, DRM)

@item WMCDC
Wireless Mobile Communication Device Class (USB, MS, Windows, CDC)

@item WMF
WikiMedia Foundation (org.)

@item WMI
Web Management Interface

@item WMI
Windows Management Instrumentation / Interface (MS, Windows, WMI, CIM)

@item WML
Wireless Markup Language (mobile-systems, WAP, XML)

@item WMM
WiFi MultiMedia (WiFi, WLAN)

@item WMP
Windows Media Player (Windows)

@item WMRM
Windows Media Rights Manager (MS, Windows)

@item WMS
Warehouse Management System (DB)

@item WMS
Web Mapping Service (OGC)

@item WMS
Workflow Management Systems

@item WMT
Web Marketing Tools [magazine] (Italy, WWW)

@item WMT
Web Marketing Tools [newsletter] (WWW)

@item WMV
Windows Media Video [CODEC] (MS, Windows, video, CODEC)

@item WMVHD
Windows Media Video - High Definition (WMV, Windows), "WMV-HD"

@item WNIM
Wide area Network Interface Module

@item WNM
Wireless Network Management (CA, Unicenter)

@item WNPP
Work-Needing and Prospective Packages (Linux, Debian)

@item WOF
Windows Overlay Filter (MS, Windows)

@item WOF
WithOut Fan (CPU)

@item WOFF
Web Open Font Format (WWW)

@item WOL
Wake On LAN (LAN, APM, BIOS)

@item WON
World-O-Networking (BeOS)

@item WOPC
Walking Optimal Power Calibration (BenQ, Philips, DVD)

@item WORDIA
WORD Internet Assistant (MS)

@item WORM
Write Once Read Many (CD)

@item WOSA
Windows Open System Architecture (MS)

@item WOSAXCEM
WOSA eXtensions for Control, Engineering and Manufacturing (WOSA, MS)

@item WOSAXFS
WOSA eXtensions for Financial Services (WOSA, MS), "WOSA X FS" 

@item WOSAXRT
WOSA eXtensions for Real-Time market data (WOSA, MS)

@item WOSC
World Organization of Systems and Cybernetics (org., France)

@item WOT
Web Of Things (W3C), "WoT"

@item WOW
Windows On Windows

@item WOW
World Of Warcraft

@item WP
Word Perfect

@item WPA
Wi-Fi Protected Access (WEP, WLAN)

@item WPAD
Web Proxy Auto Discovery (MS, Windows)

@item WPAN
Wireless Personal Area Network

@item WPBT
Windows Platform Binary Table (MS, Windows, BIOS, UEFI)

@item WPE
Windows Protected Environment

@item WPF
Windows Presentation Foundation (MS, Windows, XAML)

@item WPFE
Windows Presentation Foundation / Everywhere (MS, Windows), "WPF/E"

@item WPS
Wi-fi Protected Setup (W-LAN, Wi-Fi)

@item WPS
Windows Printing System (MS, Windows)

@item WPS
Wireless Portal Suite (WLAN)

@item WPS
Word Processing Software

@item WPS
WorkPlace Shell (OS/2, IBM, Shell)

@item WQE
Work Queue Element (Infiniband, CA, QP)

@item WQHD
Wide Quad HD [2560x1440] (HD)

@item WQL
WMI Query Language (MS, Windows, WMI)

@item WQSXGA
Wide Quad Super XGA [3200x2048] (XGA)

@item WQUXGA
Wide Quad Ultra XGA [3840x2400] (XGA, QWUXGA)

@item WQVGA
Wide Quarter VGA [432x240] (QVGA, VGA)

@item WQXGA
Wide Quad XGA [2560x1600] (XGA)

@item WRAM
Window Random Access Memory (RAM, IC, Samsung, Matrox)

@item WRB
Web Request Broker (Oracle, WWW)

@item WRSF
Web Service Resource Language (grid)

@item WRT
Whitewater Resource Toolkit (Windows, TPW)

@item WRT
With Respect To (slang, Usenet, IRC)

@item WS
[Red Hat Enterprise Linux] WorkStation (RedHat, Linux, RHEL)

@item WS
Web Server (Corel)

@item WS4B
Web Services for Browser (WWW)

@item WSA
Wafer Supply Agreement (AMD)

@item WSAD
Websphere Studio Application Developper (IBM, Java)

@item WSCM
Web Services Component Model (OASIS, WWW)

@item WSDD
Web Service Deployment Descriptor (WWW, XML)

@item WSDK
Websphere Software Development Kit (IBM, Java)

@item WSDL
Web Services Description Language (WWW, XML)

@item WSDP
[Java] Web Services Developer Pack (WWW, XML, Java, Sun)

@item WSE
Web Service Enhancements (MS, WWW)

@item WSEB
Warp Server E-Business (IBM, OS/2), "WSeb"

@item WSF
Work Station Feature (IBM)

@item WSF
Work Station Function (IN)

@item WSFL
Web Services Flow Language (WWW, IBM)

@item WSH
Windows Scripting Host (MS, Windows, COM, WSH)

@item WSI
Web Services Interoperability (org., OASIS, WWW), "WS-I"

@item WSIA
Web Services for Interactive Applications (OASIS, WWW)

@item WSIF
Web Services Invocation Framework

@item WSIL
Web Services Inspection Language (IBM, XML)

@item WSIS
World Summit on the Information Society

@item WSL
Windows Subsystem for Linux (MS, Windows, Linux)

@item WSM
Wireless Site Management

@item WSP
White SPace [character] (ABNF, RFC 2234)

@item WSP
Wireless Session Protocol (WAP, mobile-systems)

@item WSP
Workstation Security Package

@item WSRM
Windows System Resource Manager (MS, Windows)

@item WSRP
Web Services for Remote Portals (WSIA, WSXL, OASIS)

@item WSS
Web Services Security (WWW)

@item WSS
Windows Sharepoint Services (MS, Windows)

@item WSS
Windows Sound System (Windows, audio)

@item WSSD
Windows Server Software-Defined (MS, Windows)

@item WSTK
Web Service ToolKit (IBM, WWW)

@item WSTKMD
Web Services Tool Kit for Mobile Devices (IBM, Java)

@item WSUS
Windows Server Update Services (MS, Windows)

@item WSVGA
Wide Super Video Graphics Adapter / Array (VGA)

@item WSVT
Web Service Validation Tools (WTP)

@item WSXGA
Wide Super eXtended Graphics Adapter / Array (XGA), "WSXGA+"

@item WSXL
Web Services Experience Language (WWW)

@item WT
Working Text (DSL-Forum, TR)

@item WTB
Want To Buy (MMORPG, slang)

@item WTC
Windows Terminal Controller

@item WTF
What / Where / Who / Why The Fuck (telecommunication, Usenet, IRC)

@item WTH
What / Where / Who / Why The Hell (telecommunication, Usenet, IRC)

@item WTLS
Wireless Transport Layer Security [protocol] (TLS, WAP)

@item WTP
Web Tool Platform (Eclipse, Java, WWW)

@item WTP
Wireless Transport Protocol (WAP, mobile-systems)

@item WTS
Want To Sell (MMORPG, slang)

@item WTS
Web Transaction Security (WWW)

@item WTS
Windows Terminal Server (MS, :NET)

@item WTT
Want To Trade (MMORPG, slang)

@item WTX
Workstation Technology eXtended [format]

@item WUXGA
Wide Ultra eXtended Graphics Adapter [1920x1200] (XGA)

@item WVGA
Wide Video Graphics Array  [800 x 480], [854 x 480], ...

@item WVNET
West Virginia Network for Educational Telecomputing (network, USA)

@item WVSO
WiederVerwendbare SoftwareObjekte

@item WWAN
Wireless Wide Area Network (WLAN, WAN, LTE)

@item WWCID
World Wide Cartridge IDentifier (IBM, Streamer, WORM)

@item WWDC
World Wide Developer Conference (Apple)

@item WWDM
Wide Wave Division Multiplex

@item WWIMS
Worldwide Warning Indicator Monitoring System (mil.)

@item WWMCCS
Worldwide Military Command and Control System (mil., predecessor, GCCS)

@item WWOLS
World Wide On-Line System (mil.)

@item WWW
World Wide Waiting (slang)

@item WWW
World Wide Web (Internet)

@item WXGA
Wide eXtended Graphics Adapter (XGA)

@item WYGIWYNTYH
What You Get Is What You Never Thought You Had (slang)

@item WYSBYGI
What You See Before You Get It (DTP)

@item WYSIWIS
What You See Is What I See

@item WYSIWYG
What You See Is What You Get (DTP)

@end table

