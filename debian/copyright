Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vera
Source: ftp://ftp.gnu.org/gnu/vera/

Files: *
Copyright: 1993-2020 Oliver Heidelbach <ohei@snafu.de>
License: GFDL-NIV-1.1+
Comment:
 Debian comments:
   V.E.R.A.'s license blurb releases it under GFDL version 1.1 or later, but the
   included license file contains GFDL version 1.3.
   .
   On Debian systems, the complete text of the GNU Free Documentation License
   can be found in "/usr/share/common-licenses/GFDL-1.3"
 Upstream comments:
   Within the above restrictions the distribution of this document is explicitly
   encouraged and I hope you'll find it of some value.
   .
   This dictionary has nothing to do with Systems Science Inc. or its products.

Files: debian/*
Copyright: 1998-2000 Carsten Leonhardt <leo@debian.org>
           2001-2005 Robert D. Hilliard <hilliard@debian.org>
           2008      Christoph Berg <myon@debian.org>
           2008      Sven Joachim <svenjoac@gmx.de>
           2013-2021 Ryan Kavanagh <rak@debian.org>
License: GFDL-NIV-1.1+
Comment:
 On Debian systems, the complete text of the GNU Free Documentation License
 version 1.2 can be found in "/usr/share/common-licenses/GFDL-1.2"
 .
 Carsten Leonhardt, Robert D. Hilliard, and Christoph Berg never declared the
 license under which their work fell. As of October 2013, the only remaining
 work from Carsten's maintainership is six short changelog entries. The work
 remaining from Robert's maintainership is:
  * the historic dict-vera changelog (debian/dict-vera.changelog-old)
  * his changelog entries in debian/changelog
  * the calls to dictdconfig and restarting dictd in debian/{postinst,postrm}
  * the calls to makeinfo and dictzip in debian/rules
  * the regexes to remove certain kinds of texinfo markup when generating the
    dictd file (debian/sedfile)
 All that remains from Christoph Berg's QA upload is his changelog entry and a
 call to dpkg-parsechangelog to debian/rules to determine the package's version.
 Since Robert, Carsten, and Christoph maintained or contributed to this package
 for Debian as Debian Developers after having agreed to abide by the DFSG, their
 works are implicitly distributable under a DFSG compliant license.
 .
 Subsequent contributors agreed to distribute their contribution to the package
 under the GFDL-1.1+.

License: GFDL-NIV-1.1+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.1 or
 any later version published by the Free Software Foundation; with no
 Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
 Texts. A copy of the license is included in the section entitled "GNU FDL".
